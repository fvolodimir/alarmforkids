package vfediuchko.kids.alarm;

import android.app.IntentService;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;

import vfediuchko.kids.alarm.ui.din_don_time.DinDonTimeActivity;

/**
 * Created by V.Fediuchko on 23.05.2016.
 */
public class AppService extends IntentService {
    public AppService() {
        super("AppService");
    }

    private MediaPlayer player;

    @Override
    protected void onHandleIntent(Intent intent) {

//        try {

        Log.d("kidsAlarm", "pi pi pi in service ");
        Intent newIntent = new Intent(this, DinDonTimeActivity.class);
        newIntent.putExtras(intent.getExtras());
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);
//            AssetFileDescriptor afd = getAssets().openFd("nu_puhody.mp3");
//            player = new MediaPlayer();
//            player.setDataSource(afd.getFileDescriptor());
//            player.prepare();
//            player.start();
//        } catch (IOException e) {
//            Log.e("AppService", "Exception appending to log file", e);
//        }
    }
}
