package vfediuchko.kids.alarm;

import android.app.Application;

import com.facebook.stetho.Stetho;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by V.Fediuchko on 01.07.2016.
 */
public class KidsAlarmApp extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
     //   Realm.deleteRealm(config);
        Realm.setDefaultConfiguration(config);
    }
}
