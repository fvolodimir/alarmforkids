package vfediuchko.kids.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.LinkedList;

import vfediuchko.kids.alarm.receivers.OnAlarmReceiver;
import vfediuchko.kids.alarm.ui.Sound;

/**
 * Created by V.Fediuchko on 30.05.2016.
 */
public class Utils {
    public static void cancelAlarm(Context context, int id) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, OnAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, intent, 0);
        alarmManager.cancel(pendingIntent);
    }

    public static LinkedList<Sound> getAllKidsAlarmSounds() {
        Sound sound;
        LinkedList<Sound> list = new LinkedList<>();
        for (int i = 0; i < 20; i++) {
            sound = new Sound();
            sound.setName("nu_puhody.mp3");
            sound.setPictureRes(R.drawable.ic_launcher);
            list.add(sound);
        }

        return list;
    }
}
