package vfediuchko.kids.alarm.db;

import io.realm.Realm;
import io.realm.RealmResults;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.ui.Sound;

/**
 * Created by V.Fediuchko on 02.08.2016.
 */
public class DbService {
    // Create test data
    private static DbService dbService;

    public static DbService getInstance() {
        if (null == dbService) {
            dbService = new DbService();
        }
        return dbService;
    }

    public void addNewAlarm(final KidsAlarm kidsAlarm) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(kidsAlarm);
            }
        });
        realm.close();
    }

    public rx.Observable<KidsAlarm> getAlarm(final int id) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(KidsAlarm.class).equalTo("id", id).findFirst().asObservable();
    }

    public rx.Observable<RealmResults<KidsAlarm>> getAlarms() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(KidsAlarm.class).findAllAsync().asObservable();
    }

    public void removeAnAlarm(final String id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<KidsAlarm> results = realm.where(KidsAlarm.class).equalTo("id", id).findAllAsync();
                results.clear();
            }
        });
    }

    public void addSounds() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (int i = 0; i < 20; i++) {
                    Sound sound = new Sound();
                    sound.setName("nu_puhody.mp3");
                    sound.setPictureRes(R.drawable.ic_launcher);
                    realm.copyToRealm(sound);
                }
            }
        });
        realm.close();
    }

    public rx.Observable<RealmResults<Sound>> getSounds() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Sound.class).findAllAsync().asObservable();
    }

    public rx.Observable<Sound> getSound(final int id) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Sound.class).equalTo("id", id).findFirst().asObservable();
    }
}
