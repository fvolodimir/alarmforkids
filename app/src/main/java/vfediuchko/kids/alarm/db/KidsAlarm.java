package vfediuchko.kids.alarm.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import vfediuchko.kids.alarm.ui.Sound;

/**
 * Created by V.Fediuchko on 31.05.2016.
 */
public class KidsAlarm extends RealmObject {

    @PrimaryKey
    private int id = RealmAutoIncrement.getInstance(this.getClass()).getNextIdFromModel();
    private Sound sound;
    private String dateStr;
    private long peTime;
    private String timeStr;
    private int repeatingCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Sound getSound() {
        return sound;
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public long getPeTime() {
        return peTime;
    }

    public void setPeTime(long peTime) {
        this.peTime = peTime;
    }

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public int getRepeatingCount() {
        return repeatingCount;
    }

    public void setRepeatingCount(int repeatingCount) {
        this.repeatingCount = repeatingCount;
    }
}
