package vfediuchko.kids.alarm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

import vfediuchko.kids.alarm.AppService;

/**
 * Created by V.Fediuchko on 23.05.2016.
 */
public class OnAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("kidsAlarm", "OnAlarmReceiver sendWakefulWork " + Calendar.getInstance().getTime());
        Intent newIntent = new Intent(context, AppService.class);
        newIntent.putExtras(intent.getExtras());
        context.startService(newIntent);
    }
}