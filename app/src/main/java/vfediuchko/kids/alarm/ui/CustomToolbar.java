package vfediuchko.kids.alarm.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import vfediuchko.kids.alarm.R;

public class CustomToolbar extends FrameLayout {
    private String label;
    private boolean hideLeft;
    private boolean hideRight;
    private String rightButtonText;
    private String leftButtonText;
    @Bind(R.id.back_image_button)
    ImageView backImageButton;
    @Bind(R.id.next_image_button)
    ImageView nextImageButton;
    @Bind(R.id.title)
    TextView titleTextView;
    @Bind(R.id.back_button_text)
    TextView buttonBack;
    @Bind(R.id.next_button_text)
    TextView buttonNext;

    public CustomToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ToolbarWrapper, 0, 0);

        label = a.getString(R.styleable.ToolbarWrapper_title_text);
        hideLeft = a.getBoolean(R.styleable.ToolbarWrapper_hide_left_section, false);
        hideRight = a.getBoolean(R.styleable.ToolbarWrapper_hide_right_section, false);
        rightButtonText = a.getString(R.styleable.ToolbarWrapper_right_section_text);
        leftButtonText = a.getString(R.styleable.ToolbarWrapper_left_section_text);
        a.recycle();
    }

    public void onViewAdded(View child) {
        super.onViewAdded(child);
        ButterKnife.bind(this);

        titleTextView.setText(label);
        if (!TextUtils.isEmpty(rightButtonText)) {
            hideRight = true;
            buttonNext.setVisibility(VISIBLE);
            buttonNext.setText(rightButtonText);
            //buttonNext.setTypeface(((PrintApplication) getContext().getApplicationContext()).getRegularTypeface());
        }
        if (!TextUtils.isEmpty(leftButtonText)) {
            hideLeft = true;
            buttonBack.setVisibility(VISIBLE);
            buttonBack.setText(leftButtonText);
         //   buttonBack.setTypeface(((PrintApplication) getContext().getApplicationContext()).getRegularTypeface());
        }
        backImageButton.setVisibility(hideLeft ? GONE : VISIBLE);
        nextImageButton.setVisibility(hideRight ? GONE : VISIBLE);
    }
}