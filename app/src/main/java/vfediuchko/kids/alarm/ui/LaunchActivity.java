package vfediuchko.kids.alarm.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import io.realm.RealmResults;
import rx.functions.Action1;
import rx.functions.Func1;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.db.DbService;
import vfediuchko.kids.alarm.db.KidsAlarm;
import vfediuchko.kids.alarm.ui.alarms.AlarmsActivity;
import vfediuchko.kids.alarm.ui.select_sound.SelectAlarmSoundActivity;

public class LaunchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkSounds();
        checkNavigation();
    }

    private void checkSounds() {//todo replace calling
        final DbService db = DbService.getInstance();
        db.getSounds()
                .subscribe(new Action1<RealmResults<Sound>>() {
                    @Override
                    public void call(RealmResults<Sound> sounds) {
                        if (sounds.size() == 0) {
                            db.addSounds();
                        }// TODO: 10.08.2016 add check - were sounds updated
                    }
                });
    }

    private void checkNavigation() {
        DbService.getInstance().getAlarms()
                .filter(new Func1<RealmResults<KidsAlarm>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<KidsAlarm> kidsAlarms) {
                        return kidsAlarms.isLoaded();
                    }
                })
                .subscribe(new Action1<RealmResults<KidsAlarm>>() {
                    @Override
                    public void call(RealmResults<KidsAlarm> kidsAlarms) {
                        if (kidsAlarms.size() > 0) {
                            Intent intent = new Intent(LaunchActivity.this, AlarmsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            View hello = findViewById(R.id.hello);
                            if (null == hello) return;
                            hello.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(LaunchActivity.this, SelectAlarmSoundActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }
}
