package vfediuchko.kids.alarm.ui;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.LinkedList;

import vfediuchko.kids.alarm.db.KidsAlarm;

/**
 * Created by V.Fediuchko on 23.05.2016.
 */
public class Prefs {
    private static final String PREFS_NAME = "KIDS_ALARM_DATA_STORAGE";
    private static final String CURRENT_REPEATING_COUNT = "CURRENT_REPEATING_COUNT";
    private static final String NEW_ADDED_ALARM = "NEW_ADDED_ALARM";
    private static final String ALARMS_LIST = "ALARMS_LIST";
    private static final String SOUNDS_VERSION_HAS_BEEN_UPDATED = "SOUNDS_VERSION_HAS_BEEN_UPDATED";

    private static Prefs instance;

    private SharedPreferences preferences;
    private static Gson gson;

    public static Prefs getInstance(Context context) {
        if (instance == null) {
            instance = new Prefs(context);
            gson = new Gson();
        }
        return instance;
    }

    private Prefs(Context context) {
        preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public int getCurrentAlarmRepeatingCount() {

        return preferences.getInt(CURRENT_REPEATING_COUNT, 0);
    }

    public void setCurrentAlarmRepeatingCount(int count) {
        preferences.edit().putInt(CURRENT_REPEATING_COUNT, count).apply();
    }

    public boolean isSoundsVersionUpdated() {

        return preferences.getBoolean(SOUNDS_VERSION_HAS_BEEN_UPDATED,false);
    }

    public void setSoundsVersionUpdated(boolean updated) {
        preferences.edit().putBoolean(SOUNDS_VERSION_HAS_BEEN_UPDATED, updated).apply();
    }
    /*public void saveAlarmsList(LinkedList<KidsAlarm> alarms) {
        preferences.edit().putString(ALARMS_LIST, gson.toJson(alarms, new TypeToken<LinkedList<KidsAlarm>>() {
        }.getType())).apply();
    }*/
/*

    public LinkedList<KidsAlarm> getAlarmsList() {
        String json = preferences.getString(ALARMS_LIST, null);
        if (json != null) {
            return gson.fromJson(json, new TypeToken<LinkedList<KidsAlarm>>() {
            }.getType());
        }

        return null;

    }
*/

  /*  public void saveNewAlarm(KidsAlarm alarm) {
        LinkedList<KidsAlarm> kidsAlarms = getAlarmsList();
        if (null == kidsAlarms) {
            kidsAlarms = new LinkedList<>();
        }
        kidsAlarms.add(alarm);
        saveAlarmsList(kidsAlarms);
    }

    public KidsAlarm getAlarmById(int id) {
        for (KidsAlarm alarm : getAlarmsList()) {
            if (alarm.getId() == id) {
                return alarm;
            }
        }
        return null;
    }
*/
   /* public void removeAlarm(int id) {
        LinkedList<KidsAlarm> updatedAlarmsList = getAlarmsList();
        int index =updatedAlarmsList.indexOf(getAlarmById(id));
        updatedAlarmsList.remove(index);
        saveAlarmsList(updatedAlarmsList);
    }*/
}
