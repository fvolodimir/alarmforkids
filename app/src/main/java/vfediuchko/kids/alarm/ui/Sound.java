package vfediuchko.kids.alarm.ui;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import vfediuchko.kids.alarm.db.RealmAutoIncrement;

/**
 * Created by V.Fediuchko on 30.05.2016.
 */
public class Sound extends RealmObject {
    @PrimaryKey
    private int id = RealmAutoIncrement.getInstance(this.getClass()).getNextIdFromModel();
    private String name;
    private int pictureRes;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPictureRes() {
        return pictureRes;
    }

    public void setPictureRes(int pictureRes) {
        this.pictureRes = pictureRes;
    }
}
