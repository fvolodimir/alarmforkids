package vfediuchko.kids.alarm.ui.alarms;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.db.KidsAlarm;

/**
 * Created by V.Fediuchko on 31.05.2016.
 */
public class AlarmViewHolder extends RecyclerView.ViewHolder {
    private OnAlarmClickListener listener;
    View itemView;
    @Bind(R.id.alarm_time)
    TextView time;
    @Bind(R.id.icon)
View icon;
    public AlarmViewHolder(View itemView, OnAlarmClickListener listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
        this.itemView = itemView;

    }

    public void setItem(final KidsAlarm alarm) {
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(alarm);
            }
        });
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemoveAlarmClick(alarm);
            }
        });
        time.setText(alarm.getTimeStr());
    }

}
