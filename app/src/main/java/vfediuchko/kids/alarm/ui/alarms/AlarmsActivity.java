package vfediuchko.kids.alarm.ui.alarms;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import rx.functions.Action1;
import rx.functions.Func1;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.db.DbService;
import vfediuchko.kids.alarm.db.KidsAlarm;
import vfediuchko.kids.alarm.receivers.OnAlarmReceiver;
import vfediuchko.kids.alarm.ui.CustomToolbar;
import vfediuchko.kids.alarm.ui.Sound;
import vfediuchko.kids.alarm.ui.details.DetailsActivity;
import vfediuchko.kids.alarm.ui.select_sound.SelectAlarmSoundActivity;

/**
 * Created by V.Fediuchko on 30.05.2016.
 */
public class AlarmsActivity extends AppCompatActivity {

    public static final String EXTRA_ALARM_ID = "extra_alarm_id";
    public static final int DEFAULT_SOUND_ID_VALUE = 100500;
    public static final int SELECT_SOUND_REQUEST_CODE = 888;
    @Bind(R.id.toolbar_wrapper)
    CustomToolbar toolbarWrapper;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    OnAlarmClickListener onAlarmClickListener = new OnAlarmClickListener() {
        @Override
        public void onItemClick(KidsAlarm alarm) {
            Intent intent = new Intent(AlarmsActivity.this, DetailsActivity.class);
            intent.putExtra(DetailsActivity.ALARM_ID, alarm.getId());
            AlarmsActivity.this.startActivity(intent);
        }

        @Override
        public void onRemoveAlarmClick(KidsAlarm alarm) {
            adapter.notifyDataSetChanged();
            Toast.makeText(AlarmsActivity.this, "item must be removed", Toast.LENGTH_LONG).show();

        }

    };
    private Calendar calendar;
    private AlarmsAdapter adapter;
    private AlarmManager alarmManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarms);
        ButterKnife.bind(this);
        // TODO: 10.08.2016 getSound
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AlarmsAdapter(AlarmsActivity.this, onAlarmClickListener);
        recyclerView.setAdapter(adapter);


        DbService.getInstance().getAlarms()
                .filter(new Func1<RealmResults<KidsAlarm>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<KidsAlarm> kidsAlarms) {
                        return kidsAlarms != null && kidsAlarms.size() != 0;
                    }
                })
                .subscribe(new Action1<RealmResults<KidsAlarm>>() {
                    @Override
                    public void call(RealmResults<KidsAlarm> kidsAlarms) {
                        adapter.setData(kidsAlarms);

                    }
                });
        calendar = Calendar.getInstance();
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        initToolBarViews();
        int soundId = getIntent().getIntExtra(SelectAlarmSoundActivity.EXTRA_SELECTED_SOUND, DEFAULT_SOUND_ID_VALUE);
        createNewAlarm(soundId);
    }

    private void initToolBarViews() {
        ((TextView) toolbarWrapper.findViewById(R.id.title)).setText(getString(R.string.alarms));
        View createAlarm = toolbarWrapper.findViewById(R.id.next_button);
        createAlarm.setVisibility(View.VISIBLE);
        createAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AlarmsActivity.this, SelectAlarmSoundActivity.class);
                intent.setAction(SelectAlarmSoundActivity.ACTION_STARTED_FOR_RESULT);
                startActivityForResult(intent, SELECT_SOUND_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SELECT_SOUND_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                int soundId = data.getIntExtra(SelectAlarmSoundActivity.EXTRA_SELECTED_SOUND, DEFAULT_SOUND_ID_VALUE);
                createNewAlarm(soundId);
            }
        }
    }

    private void startDataPicker(Sound sound) {
        final KidsAlarm newAlarm = new KidsAlarm();
        newAlarm.setSound(sound);
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Calendar newAlarmCalendar = Calendar.getInstance();
                        newAlarmCalendar.set(Calendar.YEAR, year);
                        newAlarmCalendar.set(Calendar.MONTH, monthOfYear);
                        newAlarmCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        startTimePicker(newAlarm, newAlarmCalendar);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    private void startTimePicker(final KidsAlarm newAlarm, final Calendar newAlarmCalendar) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        // newAlarmCalendar.getTimeInMillis()
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog tpd = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        newAlarmCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        newAlarmCalendar.set(Calendar.MINUTE, minute);
                        newAlarmCalendar.set(Calendar.SECOND, 0);

                        newAlarm.setTimeStr(newAlarmCalendar.getTime().toString());
                        newAlarm.setPeTime(newAlarmCalendar.getTimeInMillis());

                        DbService.getInstance().addNewAlarm(newAlarm);
                        adapter.notifyDataSetChanged();
                        startNewAlarm(newAlarm);
                        // startNewAlarm(newAlarmCalendar.getTimeInMillis() + newAlarmCalendar.getTimeZone().getOffset(newAlarmCalendar.getTimeInMillis()));
                    }
                }, hour, minute, false);
        tpd.show();
    }

    private void startNewAlarm(KidsAlarm alarm) {


       /* Intent intent = new Intent(AlarmsActivity.this, OnBootReceiver.class);
        intent.setAction(OnBootReceiver.ACTION_START_ALARM);
        intent.putExtra(OnBootReceiver.ALARM_TIME, timeAt);
        sendBroadcast(intent);*/
        Log.d("kidsAlarm", "current " + Calendar.getInstance().getTime() + " " + Calendar.getInstance().getTimeInMillis());

        Intent intent = new Intent(AlarmsActivity.this, OnAlarmReceiver.class);
        intent.putExtra(EXTRA_ALARM_ID, alarm.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(AlarmsActivity.this, alarm.getId(), intent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getPeTime(), pendingIntent);
    }

    private void createNewAlarm(int soundId) {
        if (soundId == DEFAULT_SOUND_ID_VALUE)
            return;
        DbService.getInstance().getSound(soundId)
                .filter(new Func1<Sound, Boolean>() {
                    @Override
                    public Boolean call(Sound sound) {
                        return sound.isLoaded() && sound.isValid();
                    }
                })
                .subscribe(new Action1<Sound>() {
                    @Override
                    public void call(Sound sound) {
                        startDataPicker(sound);
                    }
                });
    }
}
