package vfediuchko.kids.alarm.ui.alarms;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import io.realm.RealmResults;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.db.KidsAlarm;

/**
 * Created by V.Fediuchko on 31.05.2016.
 */
public class AlarmsAdapter extends RecyclerView.Adapter<AlarmViewHolder> {
    private LayoutInflater layoutInflater;
    private RealmResults<KidsAlarm> data;
    private OnAlarmClickListener listener;

    public AlarmsAdapter(Context context, OnAlarmClickListener listener) {
        layoutInflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    public void setData(RealmResults<KidsAlarm> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public AlarmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AlarmViewHolder(layoutInflater.inflate(R.layout.item_alarm, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(AlarmViewHolder holder, int position) {
        holder.setItem(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }
}
