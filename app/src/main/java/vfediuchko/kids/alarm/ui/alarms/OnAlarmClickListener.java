package vfediuchko.kids.alarm.ui.alarms;

import vfediuchko.kids.alarm.db.KidsAlarm;

/**
 * Created by V.Fediuchko on 31.05.2016.
 */
public interface OnAlarmClickListener {
    void onItemClick(KidsAlarm alarm);
    void onRemoveAlarmClick(KidsAlarm alarm);
}
