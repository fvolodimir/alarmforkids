package vfediuchko.kids.alarm.ui.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.functions.Action1;
import rx.functions.Func1;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.db.DbService;
import vfediuchko.kids.alarm.db.KidsAlarm;
import vfediuchko.kids.alarm.ui.CustomToolbar;

/**
 * Created by V.Fediuchko on 09.08.2016.
 */
public class DetailsActivity extends AppCompatActivity {
    public static final String ALARM_ID = "alarm_id";
    @Bind(R.id.alarm_time)
    TextView alarmTime;
    @Bind(R.id.alarm_sound)
    TextView alarmSound;
    @Bind(R.id.alarm_volume)
    TextView alarmVolume;
    @Bind(R.id.toolbar_wrapper)
    CustomToolbar toolbarWrapper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        initToolbar();
        int id = getIntent().getIntExtra(ALARM_ID, 100500);
        DbService.getInstance().getAlarm(id)
                .filter(new Func1<KidsAlarm, Boolean>() {
                    @Override
                    public Boolean call(KidsAlarm alarm) {
                        return alarm.isLoaded() && alarm.isValid();
                    }
                })
                .subscribe(new Action1<KidsAlarm>() {
                    @Override
                    public void call(KidsAlarm alarm) {
                        updateUI(alarm);
                    }
                });
    }

    private void updateUI(KidsAlarm alarm) {
        alarmTime.setText(alarm.getTimeStr());
        alarmSound.setText(alarm.getSound().getName());
        alarmVolume.setText("Volume - 100%");
    }

    private void initToolbar() {
        ((TextView) toolbarWrapper.findViewById(R.id.title)).setText(getString(R.string.alarm_details));
        View goBack = toolbarWrapper.findViewById(R.id.back_button);
        goBack.setVisibility(View.VISIBLE);
        goBack.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                }
        );
    }
}
