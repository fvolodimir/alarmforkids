package vfediuchko.kids.alarm.ui.din_don_time;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.Utils;
import vfediuchko.kids.alarm.ui.alarms.AlarmsActivity;

/**
 * Created by V.Fediuchko on 30.06.2016.
 */
public class DinDonTimeActivity extends AppCompatActivity {
    int alarmId;
    @Bind(R.id.cancel)
    TextView cancel;
    @Bind(R.id.later)
    TextView later;

    @OnClick(R.id.cancel)
    void onCancel() {
        Utils.cancelAlarm(this, alarmId);
        finish();
    }

    @OnClick(R.id.later)
    void onLater() {
        Utils.cancelAlarm(this, alarmId);
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_din_don_time);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        alarmId = intent.getIntExtra(AlarmsActivity.EXTRA_ALARM_ID, 0);

    }
}