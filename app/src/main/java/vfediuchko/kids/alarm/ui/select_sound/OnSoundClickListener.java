package vfediuchko.kids.alarm.ui.select_sound;

import vfediuchko.kids.alarm.ui.Sound;

/**
 * Created by V.Fediuchko on 30.05.2016.
 */
public interface OnSoundClickListener {
    void onClick(Sound sound);

}
