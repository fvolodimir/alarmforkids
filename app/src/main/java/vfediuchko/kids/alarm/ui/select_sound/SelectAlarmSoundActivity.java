package vfediuchko.kids.alarm.ui.select_sound;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import rx.functions.Action1;
import rx.functions.Func1;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.db.DbService;
import vfediuchko.kids.alarm.ui.CustomToolbar;
import vfediuchko.kids.alarm.ui.Sound;
import vfediuchko.kids.alarm.ui.alarms.AlarmsActivity;

/**
 * Created by V.Fediuchko on 30.05.2016.
 */
public class SelectAlarmSoundActivity extends AppCompatActivity {
    private static final int COLUMNS_NUMBER = 4;
    public static final String EXTRA_SELECTED_SOUND = "extra_selected_sound";
    public static final String ACTION_STARTED_FOR_RESULT = "ACTION_STARTED_FOR_RESULT";
    private boolean startedForResultFlag;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.toolbar_wrapper)
    CustomToolbar toolbarWrapper;
    private OnSoundClickListener onSoundClickListener = new OnSoundClickListener() {
        @Override
        public void onClick(Sound sound) {
            if (startedForResultFlag) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(EXTRA_SELECTED_SOUND, sound.getId());
                setResult(RESULT_OK, returnIntent);
                finish();
            } else {
                Intent intent = new Intent(SelectAlarmSoundActivity.this, AlarmsActivity.class);
                intent.putExtra(EXTRA_SELECTED_SOUND, sound.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                SelectAlarmSoundActivity.this.startActivity(intent);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_sound);
        ButterKnife.bind(this);
        String action = getIntent().getAction();
        startedForResultFlag = action != null && action.equals(ACTION_STARTED_FOR_RESULT);
        recyclerView.setLayoutManager(new GridLayoutManager(this, COLUMNS_NUMBER));
        recyclerView.setItemAnimator(null);
        final SoundsAdapter adapter = new SoundsAdapter(this, onSoundClickListener);
        recyclerView.setAdapter(adapter);

        DbService.getInstance().getSounds()
                .filter(new Func1<RealmResults<Sound>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<Sound> sounds) {
                        return sounds != null;
                    }

                })
                .subscribe(new Action1<RealmResults<Sound>>() {
                    @Override
                    public void call(RealmResults<Sound> sounds) {
                        adapter.setData(sounds);
                    }
                });

        ((TextView) toolbarWrapper.findViewById(R.id.title)).setText(getString(R.string.select_sound));
    }
}
