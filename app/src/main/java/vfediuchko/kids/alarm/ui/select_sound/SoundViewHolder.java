package vfediuchko.kids.alarm.ui.select_sound;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.nio.Buffer;

import butterknife.Bind;
import butterknife.ButterKnife;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.ui.Sound;

/**
 * Created by V.Fediuchko on 30.05.2016.
 */
public class SoundViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.image)
    ImageView imageView;
    OnSoundClickListener onSoundClickListener;

    public SoundViewHolder(View itemView, OnSoundClickListener onSoundClickListener) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.onSoundClickListener = onSoundClickListener;
    }

    public void setItem(final Sound sound) {
        imageView.setImageResource(sound.getPictureRes());
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSoundClickListener.onClick(sound);
            }
        });
    }
}
