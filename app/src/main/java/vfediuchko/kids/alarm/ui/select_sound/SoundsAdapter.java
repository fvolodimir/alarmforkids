package vfediuchko.kids.alarm.ui.select_sound;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;

import io.realm.RealmResults;
import vfediuchko.kids.alarm.R;
import vfediuchko.kids.alarm.ui.Sound;

/**
 * Created by V.Fediuchko on 30.05.2016.
 */
public class SoundsAdapter extends RecyclerView.Adapter<SoundViewHolder> {
    private LayoutInflater layoutInflater;
    private RealmResults<Sound> data;
    OnSoundClickListener onSoundClickListener;

    public SoundsAdapter(Context context, OnSoundClickListener onSoundClickListener) {
        layoutInflater = LayoutInflater.from(context);
        this.onSoundClickListener = onSoundClickListener;
    }

    public void setData(RealmResults<Sound> data) {
        this.data = data;

    }

    @Override
    public SoundViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_view_sound, parent, false);
        return new SoundViewHolder(view, onSoundClickListener);
    }

    @Override
    public void onBindViewHolder(SoundViewHolder holder, int position) {
        holder.setItem(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

}
